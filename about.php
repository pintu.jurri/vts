<?php
   
?>
<!DOCTYPE html>
<html lang="en">
	<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/nav_bar.css" />
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>
	  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="wrapper">
 	<header>
  		<nav>
  		<ul class="main_menu">
  			<li><a href="index.php">VehcileTrak</a></li>
  			<li><a href="">About Us</a></li>
  			<li><a href="#">Contact Us</a></li>
  		</ul>
  	</nav>	
  	</header>
  	
 </div>
 <div class="container" style="background-color: white">
 	<div class="row">
 		<h1>About Free Vehicle Tracking</h1>
 		<div class="six columns">
 			This no cost, no contract Vehicle Tracking Platform provides users with real time vehicle tracking and a 7-day history report for up to 5 vehicles on one account.
			<p></p>
			<p>Use Free Vehicle Tracking to:</p>
			
			<ul>
				<li>Assist in the recovery of stolen vehicles</li>
				<li>Monitor the activities of a learner driver</li>
				<li>Have peace of mind that your precious cargo is safe</li>
			</ul>
			<p>For anyone who has been considering GPS vehicle tracking but hasn’t wanted to pay a monthly fee, or sign up to a contractual commitment, this gives them the opportunity to give it a go…for free for personal use. </p>
			<p>There are no restrictions on the usage, and no limited time frames apply to the service being free. We simply want to see as many vehicle users take advantage of this revolutionary product as possible. You will need to purchase hardware (below) and provide your own data SIM card.</p>
 		</div>
 	</div>
 </div>
</body>
</html>