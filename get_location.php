<?php
	 require_once 'includes/config.php';
	// Initialize the session
	session_start();
	if(isset($_GET['q'])){
		$username = $_GET['q'] ;	
	}else {
		$username = "";
	}
    $username = trim($username);

    try {
      
        $query = "SELECT lat,lng,speed FROM locationInfo WHERE userId = (SELECT product_no FROM users WHERE username = '$username') ORDER BY id DESC LIMIT 1;";
        $result = mysqli_query($conn, $query) or die(mysqli_error($conn));
        //Fetch as array
     	 if(mysqli_num_rows($result) > 0 ){
      	  while($row = mysqli_fetch_assoc($result)) {
			echo json_encode(
			array("lat" => $row['lat'],"lng"=>$row['lng'],"speed"=>$row['speed'])
			);  		
		 }
      } else {
      	echo json_encode(
		 array('message' => "No data found"));
      } 
    } catch (Exception $e) {
        echo $e->getMessage();
    }
	mysqli_close($conn); 
?>