<?php
	//include the db config file
	require_once "includes/config.php";
	// Define variables and initialize with empty values
	$username = $password = "";
	$username_err = $password_err = "";
	if(isset($_POST['submit'])){
	
		$username = trim($_POST["username"]);
		$password = trim($_POST['password']);
		//Validate Credentials
		// Prepare a select statement
        $sql = "SELECT username, password FROM users WHERE username = ?";
		if($stmt=mysqli_prepare($conn, $sql)){
            // Bind variables to the prepared statement as parameters
            mysqli_stmt_bind_param($stmt, "s", $param_username);
            // Set parameters
            $param_username = $username;
			//Execute prepared statment
			if(mysqli_stmt_execute($stmt)){
                // Store result
                mysqli_stmt_store_result($stmt);
				//if username exists
				if(mysqli_stmt_num_rows($stmt)==1){
                    // Bind result variables
                    mysqli_stmt_bind_result($stmt, $username, $hashed_password);
					if(mysqli_stmt_fetch($stmt)){
						
						if(password_verify($password,$hashed_password)){
					        /* Password is correct, so start a new session and
                            save the username to the session */
                           
                            session_start();
                            $_SESSION['username'] = $username;      
                            header("location: welcome.php");
						}else {
	                        // Display an error message if password is not valid
                            $password_err = 'The password you entered was not valid.';
					
						}
					}
				}else {
					// Display an error message if username doesn't exist
                    $username_err = 'No account found with that username.';
  				
					
				}
			}
			mysqli_stmt_close($stmt);
		}
		//close connection
		mysqli_close($conn);	
	}
?>

<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
   <link rel="stylesheet" href="css/nav_bar.css" />
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
 <div id="wrapper">
 	<header>
	  	<nav>
	  		<ul class="main_menu">
	  			<li><a href="index.php">VehicleTrak</a></li>
	  			<li><a href="about.php">About Us</a></li>
	  			<li><a href="#">Contact Us</a></li>
	  		</ul>
	  	</nav>			
  	</header>
  
 </div>
  
  <div class="container" style="background-color: white">
    <form id="registration" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" autocomplete="on">
    	<div class="row">
    		<div class="four columns">
    		<label for="usernameInput">Username:</label>
    		<input class="u-full-width" placeholder="username" id="username" name="username" type="text" maxlength="10" required="">
    		<span class="help-block" style="background-color:red"><?php echo $username_err; ?></span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="four columns">
    		<label for="passwordInput">Password:</label>
    		<input class="u-full-width" placeholder="password" id="password" name="password" type="password"  autocomplete="off" pattern=".{5,12}" required title="5 to 12 characters">
    		<span class="help-block" style="background-color:red"><?php echo $password_err; ?></span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="two column">
    			<input class="button-primary" value="Login" name="submit" type="submit">
    			<a class="button button-primary" href="forgot_password.php">FORGOT PASSWORD</a>
    		</div>
    		<div class="ten columns">
    			<div class="row">
    				<div class="three columns">
    					<label for="loginAccount">Not have an account</label>		
    				</div>
    				<div class="one column">
    					<a class="button button-primary" href="register.php">Register</a>	
    				</div>
    			</div>
    		
    		</div>
		</div>
    	
    </form>
  </div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/form-validation.js"></script>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
