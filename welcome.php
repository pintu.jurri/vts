<?php
    // Initialize the session
	session_start();
	// If session variable is not set it will redirect to login page
	if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  		header("location: login.php");
  		exit;
	}
	
?>
<!DOCTYPE html>

<html lang="en">

<head>

    <meta charset="UTF-8">

    <title>Welcome</title>
     <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
   <link rel="stylesheet" href="css/welcome_bar.css" />
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>

<body>
	<!-- Navigation bar for welcome page -->
 <div class="sidenav">
	  		<ul class="main_menu">
	  			<li><a href="index.php">VehicleTrak</a></li>
	  			<li><a href="">DashBoard</a></li>
	  			<li><a href="track_my.php">Track Your Vehcile</a></li>
	  			<li><a href="#">About Us</a></li>
	  			<li><a href="#">Contact Us</a></li>
	  			<li><a href="user_profile.php">Profile</a></li>
	  		</ul>
 </div>
 <!-- Rest of body for welcome page-->
 <div class="container">
 	 <div class="page-header">

        <h1>Hi, <b><?php echo $_SESSION['username']; ?></b>. Welcome to our site.</h1>

    </div>
    <div class="row">
    	<p><a class="button button-primary" style="background-color: red" href="logout.php">Sign Out</a></p>
    </div>
 </div>

</body>

</html>