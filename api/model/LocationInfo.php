<?php
    /* 
A domain Class to demonstrate RESTful web services
*/

Class LocationInfo {
	
	//DB Connection and table name
	public $conn;
	private $table_name = "locationInfo"; 		
	/*
		you should hookup the DAO here
	*/
	// object properties
	public $id;
	public $lat;
	public $lng;
	public $userId;
	public $timestamp;
	
	//constructor 
	public function __construct($db){
		$this->conn = $db;
	}
	//Getters
	public function getLocationInfo(){
		// select all query
	    $query = "SELECT * FROM " . $this->table_name; 
	             
	    // prepare query statement
	    $stmt = $this->conn->prepare($query);
	 
	    // execute query
	    $stmt->execute();
	 
	    return $stmt;
	}	
	//Setters
	public function setuserId($id){
		$this->userId = $id; 
	}
	public function setLat($lat){
		$this->lat = $lat; 
	}
	public function setLng($lng){
		$this->lng = $lng; 
	}
	
	public function setLocationInfo(){
		//query to insert record
		$query="";
		$query1 = "SELECT * FROM `mydb`.`users` WHERE product_no ='$this->userId';";
		if(mysqli_query($this->conn, $query1)){
			if(mysqli_affected_rows($this->conn)>=1){
				$query = "INSERT INTO `mydb`.`locationInfo`(id,lat,lng,userId,timestamp) values(NULL,'$this->lat','$this->lng','$this->userId',CURRENT_TIMESTAMP);";		
			}
		}
		
		//excute query
		if(mysqli_query($this->conn, $query)){
			return true;
		}
		return false;
	}
}
?>