<?php
  //Required headers
  header("Access-Control-Allow-Origin:*");
  header("Content-Type:application/json;charset=UTF-8");
  //include database and object files
  include_once '../includes/config.php';
  include_once 'model/LocationInfo.php';
  
  //initalize object
  $locationInfo =new LocationInfo($conn);
  //query location
  $stmt = $locationInfo->getLocationInfo();
  $num = $stmt->num_rows;
  
  //if more than one record
  if($num >0){
  	//gps coordinates in array
  	$gpsInfo_arr = array();
	$gpsInfo_arr["records"] =array();
	//TODO add another array
	while($row=$stmt->fetch(PDO::FETCH_ASSOC)){
		//extract row
		//this will make $row['name] to
		//just $name only
		extract($row);
		$gpsInfo_item = array(
			"id"	=>$id,
			"lat"	=>$lat,
			"lng"	=>$lng,
			"timestamp"	=>$timestamp
		);
		array_push($gpsInfo_arr["records"],$gpsInfo_item);		
	}
	echo json_encode($gpsInfo_arr);
	
  }else {
  	echo json_encode(
		array("message" => "No record found")
	);
  }
  
  
?>