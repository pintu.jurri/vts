<?php
    // required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

//get database connection
include_once '../includes/config.php';
//locationInfo
include_once 'model/LocationInfo.php';

$gpsInfo = new LocationInfo($conn);
//get posted data
//$data = json_decode(file_get_contents("php://input",true));
//set property
$gpsInfo->setuserId($_POST['userId']);
$gpsInfo->setLat($_POST['lat']);
$gpsInfo->setLng($_POST['lng']);
//$gpsInfo->timestamp = time();
//Create data
if($gpsInfo->setLocationInfo()){
	echo json_encode(
		array("message" => " record created")
	);
}else {
	echo json_encode(
		array("message" => mysqli_error($conn)
		)
	);
}


?>