<?php
    
?>
<!DOCTYPE html>
<html lang="en">
	<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/nav_bar.css" />
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>
	  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="wrapper">
 	<header>
  		<nav>
  		<ul class="main_menu">
  			<li><a href="index.php">VehcileTrak</a></li>
  			<li><a href="#">About Us</a></li>
  			<li><a href="#">Contact Us</a></li>
  		</ul>
  	</nav>	
  	</header>
  	
 </div>
 <div class="container" style="background-color: white">
 	<form id="forgotPassword" method="get" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" autocomplete="on" >
		<div class="row">
			<div class="six columns">
				<label for="emailIdInput">Enter your registered email id to reset password:</label>
				<input class="u-full-width" placeholder="email" id="email" name="email" type="email" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{1,}[.]{1}[a-zA-Z0-9]{2,}" required="Enter valid EMAIL-ID">
			</div>
		</div>
		<div class="row">
			<div class="three columns">
				<input class="button-primary" value="RESET PASSWORD" name="submit" type="submit">
			</div>
		</div> 		
 	</form>
 </div>
</body>
</html>