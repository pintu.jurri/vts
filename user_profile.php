<?php
    // Initialize the session
	session_start();
	// If session variable is not set it will redirect to login page
	if(!isset($_SESSION['username']) || empty($_SESSION['username'])){
  		header("location: login.php");
  		exit;
	}
	
?>
<!DOCTYPE html>

<html lang="en">

<head>

   <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
   <link rel="stylesheet" href="css/welcome_bar.css" />
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">
<script>
	

	var validatePassword = function(){
		var password = document.getElementById("password")
  , confirm_password = document.getElementById("confirm_password");
	  if(password.value != confirm_password.value) {
	    confirm_password.setCustomValidity("Passwords Don't Match");
	  } else {
	    confirm_password.setCustomValidity('');
	  }
	}
	
	//password.onchange = validatePassword;
	//confirm_password.onkeyup = validatePassword;
</script>
</head>

<body>
	<!-- Navigation bar for welcome page -->
 <div class="sidenav">
	  		<ul class="main_menu">
	  			<li><a href="index.php">VehicleTrak</a></li>
	  			<li><a href="welcome.php">DashBoard</a></li>
	  			<li><a href="track_my.php">Track Your Vehicle</a></li>
	  			<li><a href="#">About Us</a></li>
	  			<li><a href="#">Contact Us</a></li>
	  			<li><a href="#">Profile</a></li>
	  		</ul>
 </div>
 <!-- Rest of body for welcome page-->
 <div class="container">
	<h1>Update Your Settings</h1>
	<div class="row">
		<h3>User Settings</h3>
		<div class="row">
			<h4>Change Password</h4>
			<fieldset>
			<form id="change_password" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" onsubmit="return validatePassword()">
	    		<div class="row">
	    		<div class="four columns">
	    			<label for="usernameInput">Enter New Password:</label>
	    			<input class="u-full-width" placeholder="Enter New Password" id="password" name="password" type="password" pattern=".{5,12}" required title="5 to 12 characters" onchange="validatePassword();">
				</div>
				<div class="four columns">
					<label for="usernameInput">Confirm Password:</label>
	    			<input class="u-full-width" placeholder="Enter New Password" id="confirm_password" name="password" type="password" required="" onkeyup="validatePassword();">
				</div>
				</div>
				<div class="row">
	    			<input class="button-primary" value="Change Password" name="submit" type="submit">
				</div>
			</form>	
			</fieldset>
		</div>
	</div>
	<div class="row">
		<h3>Map Settings</h3>
	</div>
	
 </div>

</body>

</html>