<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/nav_bar.css" />
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body style="background-color: aqua">

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="wrapper">
 	<header>
  		<nav>
  		<ul class="main_menu">
  			<li><a href="">VehicleTrak</a></li>
  			<li><a href="about.php">About Us</a></li>
  			<li><a href="#">Contact Us</a></li>
  		</ul>
  	</nav>		
  	</header>
 </div>
  <div class="container">
  <div class="row">
  	<div class="eight columns">
  		<h1>Reliable smartphone based vehicle tracking </h1>
   		<h6>Locate, manage, reimburse and measure your fleet with the most advanced and reliable GPS tracking app for your business </h6>	
  	</div>
  </div>
  <div class="row">
  	<a class="button button-primary" href="register.php">Get Started for free</a>
  </div>
    
  </div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/form-validation.js"></script>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
