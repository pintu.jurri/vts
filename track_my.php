<?php
	require_once "includes/config.php";
    define("API_KEY","AIzaSyCZ-jyf8_IpeG2sQOToVspyAUAmg_k1oK0");
		// Initialize the session
	session_start();
	if(isset($_SESSION['username'])){
		$username = $_SESSION['username'] ;	
	}
	
	 $query = "select speed from locationInfo";
		$speed = 0;
		echo "<tr>";
		$result = mysqli_query($conn, $query) or die(mysqli_error($conn));
		if($result){
			if(mysqli_num_rows($result) > 0 ){
	      	  while($row = mysqli_fetch_assoc($result)) {
				 	$speed = $row['speed'];
			  	}
			}
		}
	mysqli_close($conn); 
?>

<!DOCTYPE html>
<html lang="en">
<head>
   <meta charset="UTF-8">
   <title>Track Your Vehicle</title>
     <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
   <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/welcome_bar.css" />
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">
  
  <script src="https://maps.googleapis.com/maps/api/js?sensor=false&key=<?php echo API_KEY; ?>"></script>
  <script>
  			var window
			var map;
			var marker;
			var latLng
			var currentLat;
			var currentLng;
			var currentSpeed=0;
			function initialize() {
				getLastLatLng();
			  latLng = new google.maps.LatLng(currentLat, currentLng)
			  var mapOptions = {
			    center: latLng,
			    zoom: 19,
			    mapTypeId: google.maps.MapTypeId.ROADMAP
			  };
			  map = new google.maps.Map(document.getElementById("map"), mapOptions);
			var icon = {
			    url: "images/car.ico", // url
			    scaledSize: new google.maps.Size(56, 56), // scaled size
			    origin: new google.maps.Point(0,0), // origin
			    anchor: new google.maps.Point(0, 0) // anchor
			};
			  marker = new google.maps.Marker({
			      icon: icon,
			      position: latLng,
			      title:"Hello World!",
			      visible: true,
			       animation:google.maps.Animation.BOUNCE
			  });
			  marker.setMap(map);
			}
			function getLastLatLng(){
				 var xmlhttp = new XMLHttpRequest();
				 xmlhttp.onreadystatechange = function() {
		            if (this.readyState == 4 && this.status == 200) {
		            	var obj = JSON.parse(this.responseText);
		            	currentLat = parseFloat(obj.lat);
	                  	 currentLng = parseFloat(obj.lng);
		            	
		            }
		        };
		        xmlhttp.open('GET', "get_location.php?q=+<?php echo $username; ?>+", true);
	       		xmlhttp.send(null);
			}
			
			
			function updateMarker(){
				 var xmlhttp = new XMLHttpRequest();
				 xmlhttp.onreadystatechange = function() {
		            if (this.readyState == 4 && this.status == 200) {
		            	var obj = JSON.parse(this.responseText);
		            	var lat = parseFloat(obj.lat);
	                  	 var lng = parseFloat(obj.lng);
	                  	currentSpeed = parseFloat(obj.speed);
	                  	
						// Find a <table> element with id="myTable":
						var table = document.getElementById("myTable");
						
						// Create an empty <tr> element and add it to the 1st position of the table:
						var row = table.rows[1];
						//var row = table.rows[2];
						// Insert new cells (<td> elements) at the 1st and 2nd position of the "new" <tr> element:
						var cell1 = row.cells[0];
						// Add some text to the new cells:
						cell1.innerHTML = currentSpeed;
						
	                  	console.log(currentSpeed); 
	                  	// Find a <table> element with id="myTable":
						//var table = document.getElementById("myTable");
	                  
	                  	 marker.setPosition(new google.maps.LatLng(lat,lng));
    					 map.panTo( new google.maps.LatLng(lat,lng) );
		            }
		        };
		        xmlhttp.open('GET', "get_location.php?q=+<?php echo $username; ?>+", true);
	       		xmlhttp.send(null);
			}
		 

			// function updateMarker() {
			   // $.post('get_locations.php',{}, function(json) {
			      // var LatLng = new google.maps.LatLng(json.lat, json.lng);
			      // marker.setPosition(LatLng);
			   // });
			// }
			// every 1 seconds
			setInterval(updateMarker,1000);
			google.maps.event.addDomListener(window, 'load', initialize);
	</script>
</head>
<body>
	<!-- Navigation bar for welcome page -->
	 <div class="sidenav">
  		<ul class="main_menu">
  			<li><a href="index.php">VehicleTrak</a></li>
  			<li><a href="welcome.php">DashBoard</a></li>
  			<li><a href="track_my.php">Track Your Vehcile</a></li>
  			<li><a href="#">About Us</a></li>
  			<li><a href="#">Contact Us</a></li>
  			<li><a href="#">Profile</a></li>
  		</ul>
	 </div>
	 <!-- Rest of body for welcome page-->
	<div class="container">
		<div id="map">
			
		</div>
	 <table id="myTable" style="width:100%">
	  <tr>
	    <th>AVG.SPEED</th>
	    <th>STATUS</th>
	    <th id="speed">ADDRESS</th>
	  </tr>	
	  	<?php 
		echo "<tr>";
	  	echo "<td>$speed</td>";
		echo "</tr>"; ?>
	</table> 
	</div>
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</body>
</html>