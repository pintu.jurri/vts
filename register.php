<?php
	include "includes/config.php";
	$username_err = "";
	$email_err ="";
	$product_err="";
	if(isset($_POST['submit'])){
		//Creating a encoded variable withou special chars
		$username = mysqli_real_escape_string($conn, $_REQUEST["username"]);
		$password = mysqli_real_escape_string($conn, $_REQUEST['password']);
		$email = mysqli_real_escape_string($conn, $_REQUEST['email']);
		$productno = $_REQUEST['productno'];	
		//Validate username
		$queryValidUsername = "SELECT * FROM users WHERE username = '$username';";
		if(mysqli_query($conn, $queryValidUsername)){
				//If there are any other users with same username
			if(mysqli_affected_rows($conn) >= 1){
				$username_err = "Username Already Taken";
			}else {
				 $username = trim($username);
			}
		}
		//Validate email-id
		$queryValidEmail = "SELECT * FROM users WHERE email='$email';";
		if(mysqli_query($conn, $queryValidEmail)){
			if(mysqli_affected_rows($conn) >=1){
				$email_err = "Email Already Exists";
			}else {
				$email  = trim($email);
			}
		}
		//Validate Product No.
		$queryValidProductNo  = "SELECT * FROM users WHERE product_no ='$productno';";
		if(mysqli_query($conn, $queryValidProductNo)){
			if(mysqli_affected_rows($conn) >=1){
				$product_err = "Product No. in use already";
			}else {
				$productno  = trim($productno);
			}
		}
		
		if(empty($username_err) && empty($email_err) && empty($product_err)){
			//For simple query
			$password = password_hash($password, PASSWORD_DEFAULT);
			
			$query  = "INSERT INTO users values('$username','$password','$email','$productno');";
			//Execture query and if successful
			if(mysqli_query($conn, $query)){
				header("location:login.php");
			}else {
				mysqli_error($conn);
			}					
		}else {
			
		}

	}
	mysqli_close($conn);
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <!-- Basic Page Needs
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta charset="utf-8">
  <title>Vehicle Tracking System</title>
  <meta name="Vehicel Tracking System to track vehicle location using gps" content="">
  <meta name="sourabh jurri" content="">

  <!-- Mobile Specific Metas
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- FONT
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600" rel="stylesheet" type="text/css">

  <!-- CSS
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="stylesheet" href="css/normalize.css">
  <link rel="stylesheet" href="css/skeleton.css">
  <link rel="stylesheet" href="css/nav_bar.css" />
  <!-- Favicon
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
  <link rel="icon" type="image/png" href="images/favicon.png">

</head>
<body>

  <!-- Primary Page Layout
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
<div id="wrapper">
 	<header>
  		<nav>
  		<ul class="main_menu">
  			<li><a href="index.php">VehcileTrak</a></li>
  			<li><a href="about.php">About Us</a></li>
  			<li><a href="#">Contact Us</a></li>
  		</ul>
  	</nav>	
  	</header>
  		
 </div>
  <div class="container" style="background-color: white">
  	<div class="row">
  		<div class="eight columns">
  			<h1>Create your VehicleTrak account</h1>
  			<h6>Monitor your vehicles in just few minutes. Our on-boarding wizard will navigate you through the setup and successfully create your VehicleTrak account.</h6>
  		</div>
  	</div>
    <form id="registration" action = "<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post" autocomplete="on">
    	<div class="row">
    		<div class="four columns">
    		<label for="usernameInput">Username:</label>
    		<input class="u-full-width" placeholder="username" id="username" name="username" type="text" maxlength="10" required="">
    		<span class="help-block"><?php echo $username_err; ?></span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="four columns">
    		<label for="passwordInput">Password:</label>
    		<input class="u-full-width" placeholder="password" id="password" name="password" type="password"  autocomplete="off" pattern=".{5,12}" required title="5 to 12 characters">
    		</div>
    	</div>
    	<div class="row">
    		<div class="four columns">
    		<label for="emailInput">Email:</label>
    		<input class="u-full-width" placeholder="email" id="email" name="email" type="email" pattern="[a-zA-Z0-9.-_]{1,}@[a-zA-Z0-9.-]{1,}[.]{1}[a-zA-Z0-9]{2,}" required="Enter valid EMAIL-ID">
    		<span class="help-block"><?php echo $email_err; ?></span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="four columns">
    		<label for="productInput">Product No:</label>
    		<input class="u-full-width" placeholder="productno" id="productno" name="productno" type="number" min="1" required="">
    			<span class="help-block"><?php echo $product_err; ?></span>
    		</div>
    	</div>
    	<div class="row">
    		<div class="two column">
    			<input class="button-primary" value="Register" name="submit" type="submit">
    		</div>
    		<div class="ten columns">
    			<div class="row">
    				<div class="three columns">
    					<label for="loginAccount">Already have an account</label>		
    				</div>
    				<div class="one column">
    					<a class="button button-primary" href="login.php">Login</a>	
    				</div>
    			</div>
    		
    		</div>
		</div>
    	
    </form>
  </div>
<script src="js/jquery-3.3.1.min.js"></script>
<script src="js/form-validation.js"></script>
<!-- End Document
  –––––––––––––––––––––––––––––––––––––––––––––––––– -->
</body>
</html>
